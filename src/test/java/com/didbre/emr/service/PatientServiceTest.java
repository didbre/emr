package com.didbre.emr.service;

import com.didbre.emr.domain.Patient;
import com.didbre.emr.repository.PatientRepository;
import com.didbre.emr.service.validator.PatientValidator;
import com.didbre.emr.service.vo.PatientVO;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;

@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PatientServiceTest {
  private PatientService patientService;

  @MockBean static PatientRepository patientRepository;

  private Patient patient;

  @BeforeAll
  void setUp() {
    patientService = new PatientService(patientRepository, new PatientValidator(patientRepository));
  }

  @BeforeEach
  void initData() {
    patient = new Patient();
    patient.setId(1L);
    patient.setFirstName("Francois");
    patient.setLastName("Pignon");
    patient.setBirthDate(new Date());
    patient.setAddressLine1("123 foo");
    patient.setCity("montreal");
    patient.setProvince("qc");
    patient.setCountry("canada");
    patient.setZipCode("1j1 2k2");
    patient.setPhoneNumber("5141234567");
    patient.setHin("BRED18107212");
  }

  @Test
  @DisplayName("Success finding a patient by is ID")
  public void findPatientById_returnPatient() throws Exception {

    doReturn(Optional.of(patient)).when(patientRepository).findById(1l);

    PatientVO patientFound = patientService.findPatientById("1");
    assertThat(patientFound.getId()).isSameAs(1L);
  }

  @Test
  @DisplayName("Error NoSuchElementException finding a patient with inexistant id.")
  public void findPatientById_errorNotFound() throws Exception {

    doReturn(Optional.of(patient)).when(patientRepository).findById(1l);

    assertThrows(
        NoSuchElementException.class,
        () -> {
          patientService.findPatientById("666");
        });
  }

  @Test
  @DisplayName("Error finding a patient wrong ID with msg No result for patient ID < 666 >")
  public void findPatientById_errorNotFoundMsg() throws Exception {

    doReturn(Optional.of(patient)).when(patientRepository).findById(1l);

    NoSuchElementException noSuchElementException = assertThrows(
        NoSuchElementException.class,
        () -> {
          patientService.findPatientById("666");
        });
    assertEquals("No result for patient ID < 666 >", noSuchElementException.getMessage());
  }


}
