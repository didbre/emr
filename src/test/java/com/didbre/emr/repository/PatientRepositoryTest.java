package com.didbre.emr.repository;

import com.didbre.emr.domain.Patient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class PatientRepositoryTest {

  @Autowired private TestEntityManager testEntityManager;
  @Autowired private PatientRepository patientRepository;

  private Patient patient;

  @BeforeEach
  void initData() {
    patient = new Patient();
    patient.setId(1L);
    patient.setFirstName("Francois");
    patient.setLastName("Pignon");
    patient.setBirthDate(new Date());
    patient.setAddressLine1("123 foo");
    patient.setCity("montreal");
    patient.setProvince("qc");
    patient.setCountry("canada");
    patient.setZipCode("1j1 2k2");
    patient.setPhoneNumber("5141234567");
    patient.setHin("BRED18107212");
  }

  @Test
  public void whenFindPatientById_thenReturnPatient() {

    testEntityManager.persistAndFlush(patient);
    Optional<Patient> found = patientRepository.findById(1L);
    assertThat(found.get().getFirstName())
        .isEqualTo(patient.getFirstName());
  }

  @Test
  public void whenFindPatientById_thenReturnEmptyResult() {
    Optional<Patient> found = patientRepository.findById(666L);
    assertThat(found.isEmpty());
  }
}
