package com.didbre.emr.common;

import org.apache.commons.beanutils.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
    Class that contains utility
 **/
public class Utils
{
    /**
     * Copy all the object from a list to a new list of another object.
     * Used BeanUtils.copyProperties for the copy process
     *
     * @param {@link List<Object>}  listDestination
     * @param {@link List<Object>} listOrigin
     * @return @link List<Object>
     * @throws Exception
     */
    public static List<Object> getBeanFrom(Object listDestination, Object listOrigin) throws Exception
    {
        for (Object source: listOrigin ) {
            Object target= new Object();
            BeanUtils.copyProperties(source , target);
            listDestination.add(target);
        }
        return listDestination;
    }
}
